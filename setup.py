import setuptools

setuptools.setup(
    name="Hook",
    version="0.0.1",
    author="Igor Vinokurov",
    author_email="i.vinokurov@artsofte.ru",
    description="pre-commit hook",
    long_description="",
    long_description_content_type="text/markdown",
    url="https://git.artsofte.ru/lsp/lsp_config_client",
    packages=setuptools.find_packages(),
    python_requires='>=3.6',
)
